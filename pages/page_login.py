from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from pages.basepage import BasePage
from selenium.webdriver.common.by import By


class LoginPage(BasePage):

    def __init__(self, driver):
        super().__init__(driver)
        self.driver = driver

    # Home page
    home_page_login_button_ByCSS = 'a[href*="login"]'
    # Login Page
    username_field_ByCSS = 'input[id="LoginForm_username"]'
    password_field_ByCSS = 'input[id="LoginForm_password"]'
    login_button_ByCSS = 'button[id="login-button"]'
    notification_icon_ByCSS = 'i[id="notifications-icon"]'

    # methods to get locators
    def click_home_page_login_button(self):
        login_button = WebDriverWait(self.driver, 30).until(
            EC.element_to_be_clickable((By.CSS_SELECTOR, self.home_page_login_button_ByCSS)))
        self.driver.execute_script("arguments[0].click();", login_button)

    def enter_username(self, username):
        username_field = WebDriverWait(self.driver, 30).until(
            EC.visibility_of_element_located((By.CSS_SELECTOR, self.username_field_ByCSS)))
        username_field.send_keys(username)

    def enter_password(self, password):
        password_field = WebDriverWait(self.driver, 30).until(
            EC.visibility_of_element_located((By.CSS_SELECTOR, self.password_field_ByCSS)))
        password_field.send_keys(password)

    def click_login_button(self):
        login_button = WebDriverWait(self.driver, 30).until(
            EC.element_to_be_clickable((By.CSS_SELECTOR, self.login_button_ByCSS)))
        # self.driver.execute_script("arguments[0].click();", login_button)
        login_button.click()

    def verify_notification_icon(self):
        WebDriverWait(self.driver, 30).until(
            EC.presence_of_element_located((By.CSS_SELECTOR, self.notification_icon_ByCSS)))
