import unittest
from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.chrome.service import Service
from selenium.webdriver import ChromeOptions


class BaseTest(unittest.TestCase):

    def setUp(self):
        opts = ChromeOptions()
        # opts.add_argument("--window-size=390,844")    # uncomment this line for mobile view
        self.base_url = "https://www.bayt.com/"
        self.driver = webdriver.Chrome(service=Service(ChromeDriverManager().install()), options=opts)
        self.driver.maximize_window()   # Comment this line for mobile view
        self.driver.get(self.base_url)
