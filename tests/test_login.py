from tests.basetest import BaseTest
from pages.page_login import LoginPage


class Login_Bayt(LoginPage):

    def __init__(self, driver):
        super().__init__(driver)
        self.driver = driver

    def login_successfully(self):
        self.click_home_page_login_button()
        self.enter_username('qa.ahmadt@gmail.com')      # use Your email
        self.enter_password('')                         # use Your Password
        self.click_login_button()
