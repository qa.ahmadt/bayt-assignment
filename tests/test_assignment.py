import time
from tests.basetest import BaseTest
from pages.page_assignment import BaytPage
from tests.test_login import Login_Bayt


class BaytAssignment(BaseTest):

    def setUp(self):
        super().setUp()
        self.login = Login_Bayt(self.driver)
        self.page = BaytPage(self.driver)

    def test_apply_for_job(self):
        self.login.login_successfully()
        self.page.take_screenshot()
        time.sleep(5)
        self.page.click_about_us_link()
        self.page.take_screenshot()
        self.page.click_first_available_job()
        self.page.take_screenshot()
        self.driver.switch_to.window(self.driver.window_handles[1])
        # verify same job is opened in new tab
        assert self.page.get_first_job_name() in 'Content Writer'
        time.sleep(5)
        self.page.click_easy_apply_button()
        self.page.take_screenshot()
        self.page.click_visa_status_dropdown()
        self.page.click_visit_visa_option_status_dropdown()
        self.page.click_apply_now_button()
        self.page.take_screenshot()

    def test_delete_account(self):
        self.login.login_successfully()
        self.page.take_screenshot()
        time.sleep(5)
        self.page.take_screenshot()
        self.page.click_three_dot_menu()
        self.page.click_account_settings_option()
        self.page.take_screenshot()
        self.page.click_delete_account_link()
        assert self.page.get_delete_my_account_heading() in 'Delete My Account'
        self.page.click_delete_my_account_button()
        self.page.take_screenshot()
        assert self.page.get_confirm_delete_modal_title() in 'Are you sure you want to delete this item?'
        self.page.take_screenshot()
        self.page.click_confirm_delete_modal_yes_button()
        time.sleep(5)
        self.login.login_successfully()
        assert self.page.get_incorrect_email_password_message() in "You've entered an incorrect email or password, please try again."
        self.page.take_screenshot()

    def test_mobile_site_apply_for_job(self):
        self.page.click_hamburger_menu_icon()
        self.login.login_successfully()
        time.sleep(5)
        self.page.take_screenshot()
        self.page.click_search_job_field()
        self.page.enter_job_title()
        self.page.click_first_job_suggestion()
        self.page.take_screenshot()
        time.sleep(3)
        self.page.click_entered_job_title()
        time.sleep(2)
        self.page.click_uae_link()
        time.sleep(2)
        self.page.click_listed_qa_job()
        self.page.click_easy_apply_button_selected_sqa_job()
        self.page.click_apply_anyway_button()
        time.sleep(3)
        self.page.click_visa_status_dropdown()
        self.page.click_visit_visa_option_status_dropdown()
        self.page.click_apply_now_button()
