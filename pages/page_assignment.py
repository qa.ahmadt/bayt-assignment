import time
from os import getcwd

from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from pages.basepage import BasePage
from selenium.webdriver.common.by import By


class BaytPage(BasePage):

    def __init__(self, driver):
        super().__init__(driver)
        self.driver = driver

    # Home page
    about_us_link_ByCSS = 'ul li a[href*="about-us"]'

    # About Us Page
    available_jobs_ByCSS = 'div[class="card-head"] h5 a'
    job_title_ByCSS = 'h1[id="job_title"]'
    easy_apply_button_ByCSS = 'span[id="apply_1"] a'
    visa_status_dropdown_ByCSS = 'input[id="applyToJobForm_visa_status__r"]'
    visit_visa_option_ByCSS = 'li[data-text="Visit Visa"]'
    apply_now_button_ByCSS = 'button[name="submit"]'

    # Delete Account
    three_dot_menu_ByCSS = 'ul[class="nav is-aux"] li[data-bayt-popover="9"] a[href="#"]'
    account_settings_option_ByCSS = 'ul[class="dropdown"] li:nth-child(1) a'
    delete_account_link_ByCSS = 'a[href*="delete-account"]'
    delete_my_account_heading_ByCSS = 'div[class="card-head"] h3'
    delete_my_account_button_ByCSS = 'button[class="btn is-danger"]'
    confirm_delete_modal_title_ByCSS = 'div[class="modal-head"] div h5:nth-child(2)'
    confirm_delete_modal_yes_button_ByCSS = '.is-danger.non-aid'
    incorrect_email_password_message_ByCSS = 'div[class="t-danger"] ul li'

    # Mobile View Apply for job
    hamburger_menu_icon_ByCSS = 'a[aria-label="Toggle"]'
    search_job_field_ByCSS = 'input[id="text_search"]'
    search_job_text_field_ByCSS = 'div[class="list-menu-title m"] div input'
    first_job_suggestion_link_ByXpath = '(//span/b[@class="t-primary"])[1]'
    entered_job_title_field_ByCSS = 'div[class="has-icon to-left"] input'
    uae_location_job_link_ByCSS = 'a[href="/en/uae/jobs/quality-assurance-engineer-jobs/"]'
    easy_apply_button_first_sqa_searched_job_ByXpath = '(//a[@class="btn is-small"])[1]'
    listed_qa_job_ByCSS = 'h2 a[href*="qa"]'
    easy_apply_button_selected_sqa_job_ByCSS = 'div[class="m m10r"] a[id="applyLink_1"]'
    apply_anyway_button_ByCSS = 'button[class="btn u-expanded-m is-inverse "]'
    location_dropdown_ByCSS = 'input[id="search_country__r"]'
    search_location_text_field_ByCSS = 'div[class="list-menu-title "] input[class="input is-small u-expanded"]'
    uae_option_in_search_location_dropdown_ByCSS = 'li[data-value="uae"]'
    search_job_button_ByCSS = 'button[id="search_icon_submit"]'

    # methods to get locators
    def click_about_us_link(self):
        element = WebDriverWait(self.driver, 60).until(
            EC.element_to_be_clickable((By.CSS_SELECTOR, self.about_us_link_ByCSS)))
        self.driver.execute_script("arguments[0].click();", element)

    def click_first_available_job(self):
        jobs = self.driver.find_elements(By.CSS_SELECTOR, self.available_jobs_ByCSS)
        for job in jobs:
            self.driver.execute_script("arguments[0].click();", job)
            break

    def get_first_job_name(self):
        actual_job_name = str(WebDriverWait(self.driver, 60).until(
            EC.visibility_of_element_located((By.CSS_SELECTOR, self.job_title_ByCSS))).text)
        return actual_job_name

    def click_easy_apply_button(self):
        easy_apply_button = WebDriverWait(self.driver, 60).until(
            EC.element_to_be_clickable((By.CSS_SELECTOR, self.easy_apply_button_ByCSS)))
        self.driver.execute_script("arguments[0].click();", easy_apply_button)

    def click_visa_status_dropdown(self):
        visa_status_dropdown = WebDriverWait(self.driver, 60).until(
            EC.element_to_be_clickable((By.CSS_SELECTOR, self.visa_status_dropdown_ByCSS)))
        visa_status_dropdown.click()

    def click_visit_visa_option_status_dropdown(self):
        visit_visa_option = WebDriverWait(self.driver, 60).until(
            EC.element_to_be_clickable((By.CSS_SELECTOR, self.visit_visa_option_ByCSS)))
        visit_visa_option.click()

    def click_apply_now_button(self):
        apply_now_button = WebDriverWait(self.driver, 60).until(
            EC.element_to_be_clickable((By.CSS_SELECTOR, self.apply_now_button_ByCSS)))
        apply_now_button.click()

    # Delete Account Methods
    def click_three_dot_menu(self):
        three_dots = WebDriverWait(self.driver, 60).until(
            EC.element_to_be_clickable((By.CSS_SELECTOR, self.three_dot_menu_ByCSS)))
        self.driver.execute_script("arguments[0].click();", three_dots)

    def click_account_settings_option(self):
        account_settings = WebDriverWait(self.driver, 60).until(
            EC.element_to_be_clickable((By.CSS_SELECTOR, self.account_settings_option_ByCSS)))
        self.driver.execute_script("arguments[0].click();", account_settings)

    def click_delete_account_link(self):
        delete_account_link = WebDriverWait(self.driver, 60).until(
            EC.element_to_be_clickable((By.CSS_SELECTOR, self.delete_account_link_ByCSS)))
        self.driver.execute_script("arguments[0].click();", delete_account_link)

    def get_delete_my_account_heading(self):
        delete_my_account_heading = str(WebDriverWait(self.driver, 60).until(
            EC.visibility_of_element_located((By.CSS_SELECTOR, self.delete_my_account_heading_ByCSS))).text)
        return delete_my_account_heading

    def click_delete_my_account_button(self):
        delete_my_account_button = WebDriverWait(self.driver, 60).until(
            EC.element_to_be_clickable((By.CSS_SELECTOR, self.delete_my_account_button_ByCSS)))
        self.driver.execute_script("arguments[0].click();", delete_my_account_button)

    def get_confirm_delete_modal_title(self):
        confirm_delete_modal_title = str(WebDriverWait(self.driver, 60).until(
            EC.visibility_of_element_located((By.CSS_SELECTOR, self.confirm_delete_modal_title_ByCSS))).text)
        return confirm_delete_modal_title

    def click_confirm_delete_modal_yes_button(self):
        confirm_delete_modal_yes_button = WebDriverWait(self.driver, 60).until(
            EC.element_to_be_clickable((By.CSS_SELECTOR, self.confirm_delete_modal_yes_button_ByCSS)))
        self.driver.execute_script("arguments[0].click();", confirm_delete_modal_yes_button)

    def get_incorrect_email_password_message(self):
        incorrect_email_password_message = str(WebDriverWait(self.driver, 60).until(
            EC.visibility_of_element_located((By.CSS_SELECTOR, self.incorrect_email_password_message_ByCSS))).text)
        return incorrect_email_password_message

    # Mobile View apply for job
    def click_hamburger_menu_icon(self):
        hamburger_menu_icon = WebDriverWait(self.driver, 60).until(
            EC.element_to_be_clickable((By.CSS_SELECTOR, self.hamburger_menu_icon_ByCSS)))
        self.driver.execute_script("arguments[0].click();", hamburger_menu_icon)

    def click_search_job_field(self):
        search_job_field = WebDriverWait(self.driver, 60).until(
            EC.element_to_be_clickable((By.CSS_SELECTOR, self.search_job_field_ByCSS)))
        search_job_field.click()

    def enter_job_title(self):
        enter_job_title = WebDriverWait(self.driver, 60).until(
            EC.element_to_be_clickable((By.CSS_SELECTOR, self.search_job_text_field_ByCSS)))
        enter_job_title.send_keys("Quality Assurance Engineer")

    def click_first_job_suggestion(self):
        first_job_suggestion_link = WebDriverWait(self.driver, 60).until(
            EC.element_to_be_clickable((By.XPATH, self.first_job_suggestion_link_ByXpath)))
        first_job_suggestion_link.click()

    # Due to Bug below 4 location scenario can't be executed
    def click_location_dropdown(self):
        location_dropdown = WebDriverWait(self.driver, 60).until(
            EC.element_to_be_clickable((By.CSS_SELECTOR, self.location_dropdown_ByCSS)))
        location_dropdown.click()

    def enter_job_location(self):
        enter_job_location = WebDriverWait(self.driver, 60).until(
            EC.element_to_be_clickable((By.CSS_SELECTOR, self.search_location_text_field_ByCSS)))
        enter_job_location.send_keys("UAE")

    def click_uae_option(self):
        uae_option = WebDriverWait(self.driver, 60).until(
            EC.element_to_be_clickable((By.CSS_SELECTOR, self.uae_option_in_search_location_dropdown_ByCSS)))
        uae_option.click()

    def click_search_job_button(self):
        search_job_button = WebDriverWait(self.driver, 60).until(
            EC.element_to_be_clickable((By.CSS_SELECTOR, self.search_job_button_ByCSS)))
        search_job_button.click()

    def click_entered_job_title(self):
        entered_job_title_field = WebDriverWait(self.driver, 60).until(
            EC.element_to_be_clickable((By.CSS_SELECTOR, self.entered_job_title_field_ByCSS)))
        entered_job_title_field.click()

    def click_uae_link(self):
        uae_location_job_link = WebDriverWait(self.driver, 60).until(
            EC.element_to_be_clickable((By.CSS_SELECTOR, self.uae_location_job_link_ByCSS)))
        uae_location_job_link.click()

    def click_easy_apply_button_first_sqa_searched_job(self):
        easy_apply_button_first_sqa_searched_job = WebDriverWait(self.driver, 60).until(
            EC.element_to_be_clickable((By.CSS_SELECTOR, self.easy_apply_button_first_sqa_searched_job_ByXpath)))
        self.driver.execute_script("arguments[0].click();", easy_apply_button_first_sqa_searched_job)

    def click_listed_qa_job(self):
        listed_qa_job = WebDriverWait(self.driver, 60).until(
            EC.element_to_be_clickable((By.CSS_SELECTOR, self.listed_qa_job_ByCSS)))
        self.driver.execute_script("arguments[0].click();", listed_qa_job)

    def click_easy_apply_button_selected_sqa_job(self):
        selected_qa_job_apply_button = WebDriverWait(self.driver, 60).until(
            EC.element_to_be_clickable((By.CSS_SELECTOR, self.easy_apply_button_selected_sqa_job_ByCSS)))
        self.driver.execute_script("arguments[0].click();", selected_qa_job_apply_button)

    def click_apply_anyway_button(self):
        apply_anyway_button = WebDriverWait(self.driver, 60).until(
            EC.element_to_be_clickable((By.CSS_SELECTOR, self.apply_anyway_button_ByCSS)))
        self.driver.execute_script("arguments[0].click();", apply_anyway_button)

    def take_screenshot(self):
        """
        Takes screenshot of the current open web page
        """
        file_name = str(round(time.time() * 1000)) + ".png"
        cwd = str(getcwd())
        screenshot_directory = str(cwd.replace('Bayt.com', 'Bayt.com\screenshots'))
        destination_file = screenshot_directory + file_name

        try:
            self.driver.save_screenshot(destination_file)
        except NotADirectoryError:
            print("Not a directory issue")
